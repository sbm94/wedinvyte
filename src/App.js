import React, { Component } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles/";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import { PersistGate } from "redux-persist/integration/react";

//Redux
import { Provider } from "react-redux";
import { store, persistor } from "./redux/store";

//import the routes
import Routes from "./routes";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#ff6860",
      main: "#d43236",
      dark: "#9b0010",
      contrastText: "#ffffff"
    },
    secondary: {
      light: "#ffdd4b",
      main: "#ffab00",
      dark: "#c67c00",
      contrastText: "#ffffff"
    }
  }
});

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <Router>
              <Routes />
            </Router>
          </PersistGate>
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;
