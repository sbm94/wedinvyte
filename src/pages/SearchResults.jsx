import React from "react";
import WideSearchForm from "../components/WideSearchForm";
import Divider from "@material-ui/core/Divider";
import WeddingCardsGrid from "../components/WeddingCardsGrid";
import CircularProgress from "@material-ui/core/CircularProgress";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

class SearchResults extends React.Component {
  render() {
    const { loading, classes } = this.props;
    return (
      <div>
        <WideSearchForm></WideSearchForm>
        <Divider variant="middle" />
        {loading ? (
          <CircularProgress
            size={60}
            color="primary"
            className={classes.progress}
          />
        ) : (
          <WeddingCardsGrid></WeddingCardsGrid>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  loading: state.UI.loading
});

const styles = theme => ({
  progress: {
    left: "50%",
    position: "relative",
    margin: "80px 0 auto 0"
  }
});

const SearchResultsWrappedWithStyles = withStyles(styles)(SearchResults);

export default withRouter(
  connect(mapStateToProps)(SearchResultsWrappedWithStyles)
);
