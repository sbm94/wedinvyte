import React from "react";
import Jumbo from "../components/Jumbo";

class Home extends React.Component {
  render() {
    return (
      <div>
        <Jumbo {...this.props} />
      </div>
    );
  }
}

export default Home;
