import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import CoupleDetails from "../components/CoupleDetails";
import CircularProgress from "@material-ui/core/CircularProgress";
import WeddingItinerary from "../components/WeddingItinerary";
import SendMessage from "../components/SendMessage";
import Grid from "@material-ui/core/Grid";

import { getWedding, getItinerary } from "../redux/actions/weddingActions";

class WeddingDetails extends Component {
  componentDidMount() {
    console.log("Wedding Details was mounted");
    const weddingId = this.props.match.params.weddingId;
    this.props.getWedding(weddingId);
    this.props.getItinerary(weddingId);
  }

  render() {
    const { loading, classes } = this.props;
    return (
      <div>
        {loading ? (
          <CircularProgress
            size={60}
            color="primary"
            className={classes.progress}
          />
        ) : (
          <div>
            <Grid container justify="space-around" spacing={2}>
              <Grid item className={classes.coupleDetailsAndItinerary}>
                <CoupleDetails wedding={this.props.wedding}></CoupleDetails>
                <WeddingItinerary></WeddingItinerary>
              </Grid>
              <Grid item>
                <SendMessage></SendMessage>
              </Grid>
            </Grid>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.UI.loading,
  wedding: state.wedding
});

const mapActionsToProps = {
  getWedding,
  getItinerary
};

const styles = theme => ({
  progress: {
    left: "50%",
    position: "relative",
    margin: "80px 0 auto 0"
  },
  coupleDetailsAndItinerary: {
    margin: "20px 15px 20px 15px"
  },
  sendMessage: {
    position: "fixed",
    left: "70%"
  }
});

const WeddingDetailWrappedWithStyles = withStyles(styles)(WeddingDetails);

export default withRouter(
  connect(mapStateToProps, mapActionsToProps)(WeddingDetailWrappedWithStyles)
);
