import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import { Typography } from "@material-ui/core";

class CoupleDetails extends Component {
  render() {
    const { wedding } = this.props.wedding;
    const { classes } = this.props;
    return (
      <div>
        <Typography className={classes.title}>
          {wedding.firstPartnerFirstName} & {wedding.secondPartnerFirstName}
        </Typography>
        <Card raised className={classes.card}>
          <CardMedia
            className={classes.cardMedia}
            component="img"
            height="500"
            image={wedding.imageUrl}
          />
        </Card>
        <Card raised className={classes.storyCard}>
          <CardContent>
            <Typography variant="h5" className={classes.storyTitle}>
              Their Story
            </Typography>
            <Typography variant="h6" className={classes.story}>
              {wedding.story}
            </Typography>
          </CardContent>
        </Card>
      </div>
    );
  }
}

const styles = theme => ({
  progress: {
    left: "50%",
    position: "relative",
    margin: "80px 0 auto 0"
  },
  title: {
    fontSize: "24px",
    fontWeight: 600
  },
  storyTitle: {
    fontWeight: 600,
    marginBottom: "10px"
  },
  card: {
    maxWidth: "450px",
    height: "350px",
    marginTop: "30px"
  },
  cardMedia: {
    maxWidth: "100%",
    height: "auto",
    objectFit: "contain"
  },
  storyCard: {
    maxWidth: "450px",
    height: "auto",
    marginTop: "30px"
  },
  story: {
    fontWeight: 400
  }
});

const CoupleDetailsWrappedWithStyles = withStyles(styles)(CoupleDetails);

export default withRouter(connect()(CoupleDetailsWrappedWithStyles));
