import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { Typography } from "@material-ui/core";

class WeddingItinerary extends Component {
  render() {
    let itineraryList = [];
    const { itinerary } = this.props.wedding;
    const { classes } = this.props;
    let itineraryDays = itinerary[0].itineraryDays;
    console.log(itinerary[0].itineraryDays);
    let theDay = 1;
    itineraryDays.map(day => {
      let eachDay = (
        <div className={classes.itineraryDay} key={itineraryDays.indexOf(day)}>
          <Typography className={classes.itineraryDayTitle}>
            Day {theDay} | {day.city}
          </Typography>
          <Typography>
            <b>Meal:</b> {day.meal}
          </Typography>
          <Typography>
            <b>Alcohol:</b> {day.alcohol ? "Yes" : "No"}
          </Typography>
          <Typography>
            <b>Description:</b> {day.description}
          </Typography>
        </div>
      );
      theDay++;
      itineraryList.push(eachDay);
    });

    return (
      <div>
        <Card raised className={classes.card}>
          <CardContent>
            <Typography variant="h5" className={classes.cardTitle}>
              Itinerary
            </Typography>
            {itineraryList}
          </CardContent>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  wedding: state.wedding
});

const styles = theme => ({
  progress: {
    left: "50%",
    position: "relative",
    margin: "80px 0 auto 0"
  },
  titleAndPicture: {
    fontSize: "24px",
    fontWeight: 600,
    margin: "20px 15px 20px 15px"
  },
  card: {
    maxWidth: "450px",
    height: "auto",
    marginTop: "30px"
  },
  cardTitle: {
    fontWeight: 600,
    marginBottom: "10px"
  },
  itineraryDayTitle: {
    fontWeight: 600,
    marginBottom: "10px"
  },
  itineraryDay: {
    marginBottom: "15px"
  }
});

const WeddingItineraryWrappedWithStyles = withStyles(styles)(WeddingItinerary);

export default connect(mapStateToProps)(WeddingItineraryWrappedWithStyles);
