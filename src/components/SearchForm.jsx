import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import TextField from "@material-ui/core/TextField";
import AddIcon from "@material-ui/icons/Add";
import Remove from "@material-ui/icons/Remove";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { withRouter } from "react-router-dom";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import dayjs from "dayjs";
import DayJsUtils from "@date-io/dayjs";

import { getWeddings, getCountries } from "../redux/actions/weddingActions";

class SearchForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fromDate: null,
      toDate: null,
      guests: 1,
      country: "All Countries"
    };
  }

  componentDidMount() {
    this.props.getCountries();
  }

  handleFromDateChange = date => {
    let fromDate = dayjs(date);
    let toDate = dayjs(this.state.toDate);
    if (fromDate.isAfter(toDate)) {
      this.setState({ fromDate: this.state.toDate });
    } else {
      this.setState({ fromDate: date });
    }
  };

  handleCountryChange = event => {
    this.setState({
      country: event.target.value
    });
  };

  handleToDateChange = date => {
    let toDate = dayjs(date);
    let fromDate = dayjs(this.state.fromDate);
    if (toDate.isBefore(fromDate)) {
      this.setState({ toDate: this.state.fromDate });
    } else {
      this.setState({ toDate: date });
    }
  };

  incrementGuest = () => {
    if (this.state.guests === 10) {
      return;
    }
    this.setState({ guests: this.state.guests + 1 });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.props.history.push({
      pathname: "/searchResults",
      search: `?fromDate=${this.state.fromDate}&toDate=${this.state.toDate}&country=${this.state.country}&guests=${this.state.guests}`
    });
  };

  nextPath = path => {
    this.props.history.push(path);
  };

  decrementGuest = () => {
    if (this.state.guests === 1) {
      return;
    }
    this.setState({ guests: this.state.guests - 1 });
  };

  render() {
    const { classes } = this.props;
    const { wedding } = this.props;
    let menuItemsOfCountries = [];
    wedding.countries.map(country => {
      let eachCountry = (
        <MenuItem key={country} value={country}>
          {country}
        </MenuItem>
      );
      menuItemsOfCountries.push(eachCountry);
    });

    return (
      <Card className={classes.card}>
        <CardContent>
          <label className={classes.title}>
            Immerse yourself in a cultural event, unlike any other.
          </label>
          <form className={classes.form} onSubmit={this.handleSubmit}>
            <MuiPickersUtilsProvider utils={DayJsUtils}>
              <Grid container justify="space-around">
                <KeyboardDatePicker
                  fullWidth
                  autoOk
                  disableToolbar
                  variant="inline"
                  format="MM-DD-YYYY"
                  margin="normal"
                  id="fromDate"
                  name="fromDate"
                  label="From"
                  value={this.state.fromDate}
                  onChange={date => this.handleFromDateChange(date)}
                  KeyboardButtonProps={{
                    "aria-label": "change date"
                  }}
                />
                <KeyboardDatePicker
                  fullWidth
                  autoOk
                  disableToolbar
                  variant="inline"
                  margin="normal"
                  id="toDate"
                  name="toDate"
                  label="To"
                  format="MM-DD-YYYY"
                  onChange={date => this.handleToDateChange(date)}
                  value={this.state.toDate}
                  KeyboardButtonProps={{
                    "aria-label": "change date"
                  }}
                />
                <Select
                  className={classes.selectCountry}
                  fullWidth
                  labelId="demo-simple-select-helper-label"
                  id="demo-simple-select-helper"
                  value={this.state.country}
                  onChange={this.handleCountryChange}
                >
                  <MenuItem value="All Countries">All Countries</MenuItem>
                  {menuItemsOfCountries}
                </Select>
                <Grid
                  className={classes.guestGrid}
                  container
                  spacing={2}
                  alignItems="center"
                >
                  <Grid item>
                    <Fab
                      variant="extended"
                      size="small"
                      color="primary"
                      aria-label="edit"
                      onClick={this.decrementGuest}
                      className={classes.plusMinusButton}
                    >
                      <Remove className={classes.plusMinusButton} />
                    </Fab>
                  </Grid>
                  <Grid className={classes.guestTextField} item>
                    <TextField
                      type="number"
                      id="standard-basic"
                      label="Guests"
                      value={this.state.guests}
                    />
                  </Grid>
                  <Grid item>
                    <Fab
                      variant="extended"
                      size="small"
                      color="primary"
                      aria-label="add"
                      onClick={this.incrementGuest}
                      className={classes.plusMinusButton}
                    >
                      <AddIcon className={classes.plusMinusButton} />
                    </Fab>
                  </Grid>
                </Grid>
              </Grid>
            </MuiPickersUtilsProvider>
            <Button
              type="submit"
              className={classes.searchButton}
              size="large"
              variant="contained"
              color="primary"
              fullWidth
            >
              Search Weddings
            </Button>
          </form>
        </CardContent>
        <CardActions></CardActions>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  wedding: state.wedding
});

const mapActionsToProps = {
  getWeddings,
  getCountries
};

const styles = theme => ({
  card: {
    display: "inline-block",
    maxWidth: "300px",
    maxHeight: "500px",
    backgroundColor: "white",
    marginTop: "100px",
    marginLeft: "40px",
    paddingRight: "20px",
    paddingLeft: "20px",
    paddingTop: "20px",
    paddingBottom: "20px",
    borderRadius: "5px"
  },

  title: {
    fontSize: 20,
    fontWeight: 900
  },
  guestGrid: {
    margin: -9,
    marginTop: 16
  },
  guestTextField: {
    maxWidth: "130px"
  },
  searchButton: {
    margin: "auto",
    marginTop: 30
  },
  selectCountry: {
    marginTop: 16
  },
  plusMinusButton: {
    maxHeight: "25px",
    maxWidth: "15px"
  }
});

const SearchFormWrappedWithStyle = withStyles(styles)(SearchForm);

export default withRouter(
  connect(mapStateToProps, mapActionsToProps)(SearchFormWrappedWithStyle)
);
