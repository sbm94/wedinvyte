import React from "react";
import { connect } from "react-redux";
import Modal from "@material-ui/core/Modal";
import LoginModal from "./Modals/LoginModal";
import SignUpModal from "./Modals/SignUpModal";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

const MODAL_TYPES = {
  loginModal: LoginModal,
  signUpModal: SignUpModal
};

class ModalRoot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: props.modalProps.open
    };
    this.closeModal = this.closeModal.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.modalProps.open !== this.props.modalProps.open) {
      this.setState({
        modalIsOpen: nextProps.modalProps.open
      });
    }
  }

  closeModal() {
    this.props.closeModal();
  }

  render() {
    if (!this.props.modalType) {
      return null;
    }

    const SpecifiedModal = MODAL_TYPES[this.props.modalType];
    return (
      <div>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.modalIsOpen}
          onClose={this.props.closeModal}
          onEscapeKeyDown={this.props.closeModal}
        >
          <div>
            <SpecifiedModal
              openOtherModal={this.props.openOtherModal}
              closeModal={this.closeModal}
              {...this.props.modalProps}
            />
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state.modalReducer
});

export default connect(mapStateToProps, null)(ModalRoot);
