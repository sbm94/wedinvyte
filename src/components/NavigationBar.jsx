import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import ModalRoot from "./ModalRoot";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import { openModal, closeModal } from "../redux/actions/modalActions";

class NavigationBar extends React.Component {
  constructor(props) {
    super(props);
  }

  closeModal = () => {
    this.props.closeModal();
  };

  openLoginModal = () => {
    this.props.openModal(
      {
        open: true,
        closeModal: this.closeModal
      },
      "loginModal"
    );
  };

  openSignUpModal = () => {
    this.props.openModal(
      {
        open: true,
        closeModal: this.closeModal
      },
      "signUpModal"
    );
  };

  goToHomePage = () => {
    this.props.history.push("/");
  };

  render() {
    const {
      location,
      user: { credentials, authenticated },
      classes
    } = this.props;

    let loginAndSignUpButtons = (
      <div>
        <Button color="inherit" onClick={this.openLoginModal}>
          Login
        </Button>
        <Button color="inherit" onClick={this.openSignUpModal}>
          Sign Up
        </Button>
      </div>
    );

    let profileButton = (
      <Button className={classes.profileButton} color="inherit">
        {credentials.firstName}
      </Button>
    );

    return (
      <div>
        <AppBar
          className={
            location.pathname === "/"
              ? classes.homeAppBarStyle
              : classes.elsewhereAppBarStyle
          }
        >
          <Toolbar>
            <Button
              className={
                location.pathname === "/"
                  ? classes.homeAppBarStyle
                  : classes.elsewhereAppBarStyle
              }
              onClick={this.goToHomePage}
            >
              <Typography variant="h6">WEDINVYTE</Typography>
            </Button>
            <Button className={classes.rightAlignButtons} color="inherit">
              Host a Wedding
            </Button>
            {authenticated ? profileButton : loginAndSignUpButtons}
          </Toolbar>
        </AppBar>
        <ModalRoot
          openOtherModal={this.props.openModal}
          closeModal={this.props.closeModal}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  UI: state.UI
});

const mapDispatchToProps = dispatch => ({
  closeModal: () => dispatch(closeModal()),
  openModal: (modalProps, modalType) => {
    dispatch(openModal({ modalProps, modalType }));
  }
});

const styles = theme => ({
  homeAppBarStyle: {
    background: "transparent",
    color: "white",
    boxShadow: "0px 0px 0px"
  },
  rightAlignButtons: {
    marginLeft: "auto"
  },
  elsewhereAppBarStyle: {
    background: "white",
    color: "#d43236"
  },
  profileButton: {
    fontWeight: 900
  }
});

const NavigationBarWrappedWithStyle = withStyles(styles)(NavigationBar);
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(NavigationBarWrappedWithStyle)
);
