import React, { Component } from "react";
import { Link } from "react-router-dom";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import dayjs from "dayjs";
import { withRouter } from "react-router-dom";

class WeddingCard extends Component {
  constructor(props) {
    super(props);
  }

  goToWeddingDetails = weddingId => {
    this.props.history.push({
      pathname: `/getWedding/${weddingId}`
    });
  };

  render() {
    const { classes } = this.props;
    const backUrl = `${this.props.location.pathname}${this.props.location.search}`;
    const { weddingInfo } = this.props;
    const weddingId = weddingInfo.weddingId;
    const fromDate = dayjs
      .unix(weddingInfo.fromDate._seconds)
      .format("MMMM D, YYYY");
    const toDate = dayjs
      .unix(weddingInfo.toDate._seconds)
      .format("MMMM D, YYYY");
    return (
      <div>
        <Card raised className={classes.card}>
          <CardMedia
            className={classes.cardMedia}
            component="img"
            height="250"
            image={weddingInfo.imageUrl}
          />
          <CardContent>
            <Typography gutterBottom variant="h6" component="h2">
              {weddingInfo.city}, {weddingInfo.country}
            </Typography>
            <Typography gutterBottom variant="h6" component="h2">
              {weddingInfo.firstPartnerFirstName} {" & "}
              {weddingInfo.secondPartnerFirstName}
            </Typography>
            <Typography
              gutterBottom
              variant="overline"
              component="h2"
              className={classes.dates}
            >
              {fromDate} to {toDate}
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              onClick={weddingId =>
                this.goToWeddingDetails(weddingInfo.weddingId)
              }
              size="small"
              color="primary"
            >
              View Details
            </Button>
          </CardActions>
          {/* <Link
            to={{ pathname: `/getWedding/${weddingId}`, query: { backUrl } }}
          >
            Hey click here
          </Link> */}
        </Card>
      </div>
    );
  }
}

const styles = theme => ({
  card: {
    maxWidth: "300px"
  },
  cardMedia: {
    maxWidth: "100%",
    height: "350px",
    objectFit: "contain"
  },
  dates: {
    fontSize: "0.70rem"
  }
});

const WeddingCardWrappedWithStyle = withStyles(styles)(WeddingCard);

export default withRouter(connect()(WeddingCardWrappedWithStyle));
