import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import TextField from "@material-ui/core/TextField";
import AddIcon from "@material-ui/icons/Add";
import Remove from "@material-ui/icons/Remove";
import dayjs from "dayjs";

class SendMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      guests: 1
    };
  }

  incrementGuest = () => {
    if (this.state.guests === 10) {
      return;
    }
    this.setState({ guests: this.state.guests + 1 });
  };

  decrementGuest = () => {
    if (this.state.guests === 1) {
      return;
    }
    this.setState({ guests: this.state.guests - 1 });
  };

  render() {
    let checkboxes = [];
    const { wedding, itinerary } = this.props.wedding;
    const { classes } = this.props;
    //let itineraryDays = itinerary[0].itineraryDays;
    const fromDate = dayjs
      .unix(wedding.fromDate._seconds)
      .format("MMMM D, YYYY");
    const toDate = dayjs.unix(wedding.toDate._seconds).format("MMMM D, YYYY");
    let theDay = 1;
    // itineraryDays.map(day => {
    //   let checkboxForEachDay = (
    //     <div key={itineraryDays.indexOf(day)}>
    //       <Grid item className={classes.checkboxItem}>
    //         <Typography>Day {theDay}</Typography>
    //         <Checkbox></Checkbox>
    //       </Grid>
    //     </div>
    //   );
    //   theDay++;
    //   checkboxes.push(checkboxForEachDay);
    // });

    return (
      <div>
        <Card raised className={classes.sendMessageCard}>
          <CardContent className={classes.messageCard}>
            <Typography gutterBottom variant="h6" align="center">
              {fromDate} to {toDate}
            </Typography>
            <br></br>
            <Typography>
              <b>Price per day: </b> ${wedding.pricePerDay}
            </Typography>
            <Typography>
              <b>Price per person: </b> ${wedding.pricePerPerson}
            </Typography>
            <br></br>
            <Typography>
              <b>Which days are you interested in attending?</b>
            </Typography>
            <Grid container jutify="center" spacing={2}>
              {checkboxes}
            </Grid>
            <Grid
              className={classes.guestGrid}
              container
              spacing={2}
              alignItems="center"
            >
              <Grid item>
                <Fab
                  variant="extended"
                  size="small"
                  color="secondary"
                  aria-label="edit"
                  onClick={this.decrementGuest}
                  className={classes.plusMinusButton}
                >
                  <Remove className={classes.plusMinusButton} />
                </Fab>
              </Grid>
              <Grid className={classes.guestTextField} item>
                <TextField
                  type="number"
                  id="standard-basic"
                  label="Guests"
                  value={this.state.guests}
                />
              </Grid>
              <Grid item>
                <Fab
                  variant="extended"
                  size="small"
                  color="secondary"
                  aria-label="add"
                  onClick={this.incrementGuest}
                  className={classes.plusMinusButton}
                >
                  <AddIcon className={classes.plusMinusButton} />
                </Fab>
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Send Message
            </Button>
          </CardContent>
        </Card>
      </div>
    );
  }
}

const styles = theme => ({
  sendMessageCard: {
    maxWidth: "450px",
    height: "auto",
    marginTop: "85px",
    left: "65%"
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  checkboxItem: {
    margin: "20px"
  }
});

const mapStateToProps = state => ({
  wedding: state.wedding
});

const SendMessageWrappedWithStyles = withStyles(styles)(SendMessage);

export default connect(mapStateToProps)(SendMessageWrappedWithStyles);
