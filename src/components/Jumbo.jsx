import React from "react";
import { Jumbotron } from "reactstrap";
import SearchForm from "../components/SearchForm";

export default class Jumbo extends React.Component {
  render() {
    return (
      <div>
        <Jumbotron className="jumbo">
          <SearchForm {...this.props}></SearchForm>
        </Jumbotron>
      </div>
    );
  }
}
