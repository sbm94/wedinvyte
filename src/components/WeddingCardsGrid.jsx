import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import WeddingCard from "./WeddingCard";
import { Typography } from "@material-ui/core";

class WeddingCardsGrid extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let weddingsList = [];
    const {
      weddings: { weddings }
    } = this.props;

    const { classes } = this.props;
    weddings.map(wedding => {
      let eachWedding = (
        <Grid className={classes.weddingCard} key={wedding.weddingId} item>
          <WeddingCard key={wedding.weddingId} weddingInfo={wedding} />
        </Grid>
      );
      weddingsList.push(eachWedding);
    });

    const noResults = (
      <Typography gutterBottom variant="h6" component="h2">
        Sorry, no weddings were found. Try again with a different date range or
        number of guests.
      </Typography>
    );
    return (
      <div>
        <Grid
          container
          className={classes.weddingCardsGrid}
          direction="row"
          justify="space-around"
          spacing={1}
        >
          {weddingsList.length > 0 ? weddingsList : noResults}
        </Grid>
      </div>
    );
  }
}

const styles = theme => ({
  weddingCardsGrid: {
    maxWidth: "1200px",
    margin: "30px auto 0 auto"
  },
  weddingCard: {
    marginTop: "30px;"
  },
  noResults: {
    color: "red"
  }
});

const mapStateToProps = state => ({
  weddings: state.wedding
});

const WeddingCardsGridWrappedWithStyle = withStyles(styles)(WeddingCardsGrid);

export default withRouter(
  connect(mapStateToProps)(WeddingCardsGridWrappedWithStyle)
);
