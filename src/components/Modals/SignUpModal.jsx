import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import { KeyboardDatePicker } from "@material-ui/pickers";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Close from "@material-ui/icons/Close";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { signUpUser } from "../../redux/actions/userActions";
import dayjs from "dayjs";
import { MuiPickersUtilsProvider } from "@material-ui/pickers/MuiPickersUtilsProvider";
import DayJsUtils from "@date-io/dayjs";

class SignUpModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      firstName: "",
      lastName: "",
      confirmPassword: "",
      birthday: dayjs(new Date()).format("MM/DD/YYYY"),
      errors: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.UI.errors) {
      this.setState({ errors: nextProps.UI.errors });
    }
  }

  validateEmail(e) {
    const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const { validate } = this.state;
    if (emailRex.test(e.target.value)) {
      validate.emailState = "has-success";
    } else {
      validate.emailState = "has-danger";
    }
    this.setState({ validate });
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleDateChange = date => {
    this.setState({ birthday: dayjs(date) });
  };

  handleSubmit = event => {
    event.preventDefault();
    const newUserData = {
      email: this.state.email,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      birthday: this.state.birthday
    };
    this.props.signUpUser(newUserData, this.props.history);
  };

  openLoginModal = () => {
    this.props.closeModal();
    this.props.openOtherModal(
      {
        open: true,
        closeModal: this.props.closeModal
      },
      "loginModal"
    );
  };

  render() {
    const {
      UI: { loading }
    } = this.props;
    const { errors } = this.state;
    const { classes } = this.props;

    return (
      <div className={classes.modalBorder}>
        <IconButton
          className={classes.closeButton}
          onClick={this.props.closeModal}
        >
          <Close></Close>
        </IconButton>
        <Container component="main" maxWidth="xs">
          <div className={classes.paper}>
            <Typography component="h1" variant="h5">
              Register
            </Typography>
            <form
              className={classes.form}
              noValidate
              onSubmit={this.handleSubmit}
            >
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="fname"
                    name="firstName"
                    variant="outlined"
                    required
                    fullWidth
                    id="firstName"
                    label="First Name"
                    autoFocus
                    helperText={errors.firstName}
                    error={errors.firstName ? true : false}
                    value={this.state.firstName}
                    onChange={this.handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="lastName"
                    label="Last Name"
                    name="lastName"
                    autoComplete="lname"
                    helperText={errors.lastName}
                    error={errors.lastName ? true : false}
                    value={this.state.lastName}
                    onChange={this.handleChange}
                  />
                </Grid>
              </Grid>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                helperText={errors.email}
                error={errors.email ? true : false}
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                value={this.state.email}
                onChange={this.handleChange}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                helperText={errors.password}
                error={errors.password ? true : false}
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={this.state.password}
                onChange={this.handleChange}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                error={errors.confirmPassword ? true : false}
                helperText={errors.confirmPassword}
                name="confirmPassword"
                label="Confirm Password"
                type="password"
                id="confirmPassword"
                autoComplete="current-password"
                value={this.state.confirmPassword}
                onChange={this.handleChange}
              />
              <MuiPickersUtilsProvider utils={DayJsUtils}>
                <KeyboardDatePicker
                  autoOk
                  disableFuture
                  fullWidth
                  margin="normal"
                  variant="inline"
                  inputVariant="outlined"
                  label="Birthday"
                  required
                  format="MM/DD/YYYY"
                  name="birthday"
                  id="birthday"
                  value={this.state.birthday}
                  InputAdornmentProps={{ position: "start" }}
                  onChange={date => this.handleDateChange(date)}
                  error={errors.birthday ? true : false}
                  helperText={errors.birthday}
                />
              </MuiPickersUtilsProvider>

              {errors.general && (
                <Typography variant="body2" className={classes.customError}>
                  {errors.general}
                </Typography>
              )}

              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Sign In
              </Button>
              <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2">
                    Forgot password?
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="#" variant="body2" onClick={this.openLoginModal}>
                    {"Already have an account? Login."}
                  </Link>
                </Grid>
              </Grid>
            </form>
          </div>
        </Container>
      </div>
    );
  }
}

// LoginModal.propTypes = {
//   loginUser: PropTypes.func.isRequired,
//   user: PropTypes.object.isRequired,
//   UI: PropTypes.object.isRequired
// };

const mapStateToProps = state => ({
  user: state.user,
  UI: state.UI
});

const mapActionsToProps = {
  signUpUser
};

const styles = theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  modalBorder: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #fff",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)"
  },
  customError: {
    color: "red",
    fontSize: "0.8rem",
    marginTop: theme.spacing(1)
  },
  closeButton: {
    float: "right"
  }
});

const SignUpModalWrappedWithStyle = withStyles(styles)(SignUpModal);
export default withRouter(
  connect(mapStateToProps, mapActionsToProps)(SignUpModalWrappedWithStyle)
);
