import React from "react";
import { withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { openModal, closeModal } from "../../redux/actions/modalActions";

const AuthRoute = Component => {
  class Auth extends React.Component {
    openLoginModal = () => {
      this.props.openModal(
        {
          open: true,
          closeModal: this.props.closeModal
        },
        "loginModal"
      );
    };

    componentDidMount() {
      if (this.props.authenticated) {
        console.log("user is authenticated");
        let prevPath = this.props.location.query.prevPath;
        console.log(prevPath);
      } else {
        console.log("User is not authenticated");
        this.props.history.push(this.props.location.query.backUrl);
        this.openLoginModal();
      }
    }

    render() {
      const { authenticated } = this.props;
      let redirect = <Redirect />;
      return <Component {...this.props} />;
    }
  }

  const mapStateToProps = state => ({
    authenticated: state.user.authenticated
  });

  const mapDispatchToProps = dispatch => ({
    closeModal: () => dispatch(closeModal()),
    openModal: (modalProps, modalType) => {
      dispatch(openModal({ modalProps, modalType }));
    }
  });

  return withRouter(connect(mapStateToProps, mapDispatchToProps)(Auth));
};

export default AuthRoute;
