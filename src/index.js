import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../src/wedinvite.css";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import "@mobiscroll/react-lite";
import "@mobiscroll/react-lite/dist/css/mobiscroll.min.css";
import * as serviceWorker from "./serviceWorker";

import { AppContainer } from "react-hot-loader";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";

//date util library
import DayJsUtils from "@date-io/dayjs";

//BrowserRouter

ReactDOM.render(
  <AppContainer>
    <MuiPickersUtilsProvider utils={DayJsUtils}>
      <App />
    </MuiPickersUtilsProvider>
  </AppContainer>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
