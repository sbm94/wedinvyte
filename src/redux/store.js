// allows you to write action creators that return a function instead of an action
import thunk from "redux-thunk";
import { applyMiddleware, createStore, combineReducers, compose } from "redux";
//reducers
import userReducer from "./reducers/userReducer";
import weddingReducer from "./reducers/weddingReducer";
import uiReducer from "./reducers/uiReducer";
import modalReducer from "./reducers/modalReducer";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

//--- MIDDLEWARE
// add middleware inside this function

const initialState = {};
const middleware = [thunk];

const persistConfig = {
  key: "root",
  // Storage Method (React Native)
  storage,
  // Whitelist (Save Specific Reducers)
  whitelist: ["wedding", "user"],
  // Blacklist (Don't Save Specific Reducers)
  blacklist: ["modalReducer", "UI"]
};

const reducers = combineReducers({
  user: userReducer,
  wedding: weddingReducer,
  UI: uiReducer,
  modalReducer: modalReducer
});

const persistedReducer = persistReducer(persistConfig, reducers);

const store = createStore(
  persistedReducer,
  initialState,
  compose(
    applyMiddleware(...middleware)
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

let persistor = persistStore(store);

export { store, persistor };
