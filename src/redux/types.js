//User reducer types
export const SET_AUTHENTICATED = "SET_AUTHENTICATED";
export const SET_UNAUTHENTICATED = "SET_UNAUTHENTICATED";
export const SET_USER = "SET_USER";
export const LOADING_USER = "LOADING_USER";

//UI reducer types
export const SET_ERRORS = "SET_ERRORS";
export const LOADING_DATA = "LOADING_DATA";
export const CLEAR_ERRORS = "CLEAR_ERRORS";
export const STOP_LOADING_DATA = "STOP_LOADING_DATA";

//Wedding reducer types
export const WEDDINGS = "WEDDINGS";
export const SET_WEDDING = "SET_WEDDING";
export const SET_COUNTRIES = "SET_COUNTRIES";
export const SET_ITINERARY = "SET_ITINERARY";

//Modal Reducer types
export const OPEN_MODAL = "OPEN_MODAL";
export const CLOSE_MODAL = "CLOSE_MODAL";
