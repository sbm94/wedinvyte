import axios from "axios";
import {
  WEDDINGS,
  SET_ERRORS,
  LOADING_DATA,
  STOP_LOADING_DATA,
  SET_COUNTRIES,
  SET_WEDDING,
  SET_ITINERARY
} from "../types";

export const getWeddings = (fromDate, toDate, guests, country) => dispatch => {
  let weddingSearchParams = {
    fromDate: fromDate,
    toDate: toDate,
    guests: guests,
    country: country
  };
  dispatch({ type: LOADING_DATA });
  axios
    .post("/getWeddings", weddingSearchParams)
    .then(response => {
      console.log(response.data);
      dispatch({
        type: WEDDINGS,
        payload: response.data
      });
      dispatch({ type: STOP_LOADING_DATA });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getWedding = weddingId => dispatch => {
  dispatch({ type: LOADING_DATA });
  axios
    .get(`/getWedding/${weddingId}`)
    .then(response => {
      console.log(response.data);
      dispatch({
        type: SET_WEDDING,
        payload: response.data
      });
      dispatch({ type: STOP_LOADING_DATA });
    })
    .catch(err => console.log(err));
};

export const getCountries = () => dispatch => {
  axios
    .get("/getCountries")
    .then(response => {
      console.log(response.data);
      dispatch({
        type: SET_COUNTRIES,
        payload: response.data
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getItinerary = weddingId => dispatch => {
  let wedding = {
    weddingId: weddingId
  };
  axios
    .post("/getItinerary", wedding)
    .then(response => {
      console.log(response.data);
      dispatch({
        type: SET_ITINERARY,
        payload: response.data
      });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    });
};
