import {
  SET_USER,
  SET_ERRORS,
  CLEAR_ERRORS,
  LOADING_DATA,
  SET_UNAUTHENTICATED,
  CLOSE_MODAL
} from "../types";
import axios from "axios";

export const loginUser = (userData, history) => dispatch => {
  console.log(userData);
  dispatch({ type: CLEAR_ERRORS });
  dispatch({ type: LOADING_DATA });
  axios
    .post("/api/login", userData)
    .then(response => {
      console.log("errors are cleared");
      setAuthorizationHeader(response.data.token);
      dispatch(getUserData());

      dispatch({ type: CLOSE_MODAL });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getUserData = () => dispatch => {
  axios
    .get("/api/user")
    .then(response => {
      console.log(response.data);
      dispatch({
        type: SET_USER,
        payload: response.data
      });
    })
    .catch(err => console.log(err));
};

export const signUpUser = (newUserData, history) => dispatch => {
  console.log(newUserData);
  dispatch({ type: CLEAR_ERRORS });
  dispatch({ type: LOADING_DATA });
  axios
    .post("/api/signUp", newUserData)
    .then(response => {
      setAuthorizationHeader(response.data.token);
      dispatch(getUserData());
      dispatch({ type: CLEAR_ERRORS });
    })
    .catch(err => {
      console.log(err);
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    });
};

const setAuthorizationHeader = token => {
  const FBIdToken = `Bearer ${token}`;
  localStorage.setItem("FBIdToken", FBIdToken);
  axios.defaults.headers.common["Authorization"] = FBIdToken;
};

export const logoutUser = () => dispatch => {
  localStorage.removeItem("FBIdToken");
  delete axios.defaults.headers.common["Authorization"];
  dispatch({ type: SET_UNAUTHENTICATED });
};
