import { OPEN_MODAL, CLOSE_MODAL } from "../types";

export const openModal = ({ modalProps, modalType }) => dispatch => {
  dispatch({
    type: OPEN_MODAL,
    modalProps,
    modalType
  });
};

export const closeModal = () => dispatch => {
  dispatch({
    type: CLOSE_MODAL
  });
};
