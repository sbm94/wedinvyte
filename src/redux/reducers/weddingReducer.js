import { WEDDINGS, SET_WEDDING, SET_COUNTRIES, SET_ITINERARY } from "../types";

const initialState = {
  countries: [],
  weddings: [],
  wedding: {},
  itinerary: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case WEDDINGS:
      return {
        ...state,
        weddings: action.payload
      };
    case SET_WEDDING:
      return {
        ...state,
        wedding: action.payload
      };
    case SET_COUNTRIES:
      return {
        ...state,
        countries: action.payload
      };
    case SET_ITINERARY:
      return {
        ...state,
        itinerary: action.payload
      };
    default:
      return state;
  }
}
