import { OPEN_MODAL, CLOSE_MODAL } from "../types";

const initialState = {
  modalType: null,
  modalProps: {
    open: false
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case OPEN_MODAL:
      return {
        modalProps: action.modalProps,
        modalType: action.modalType,
        type: action.type
      };
    case CLOSE_MODAL:
      return initialState;
    default:
      return state;
  }
};
