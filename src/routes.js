import React, { Fragment } from "react";
import { Switch, Route } from "react-router-dom";
//import the components
import jwtDecode from "jwt-decode";

import SearchResults from "./pages/SearchResults";
import Home from "./pages/Home";
import NavigationBar from "./components/NavigationBar";
import WeddingDetails from "./pages/WeddingDetails";
import Conversation from "./pages/Conversation";
import axios from "axios";
import { store } from "./redux/store";
import { SET_AUTHENTICATED } from "./redux/types";
import { logoutUser, getUserData } from "./redux/actions/userActions";

import Auth from "./components/util/Auth";

axios.defaults.baseURL = "https://wedinvite-test.firebaseapp.com/api";

const token = localStorage.FBIdToken;
if (token) {
  const decodedToken = jwtDecode(token);
  console.log(decodedToken);
  if (decodedToken.exp * 1000 < Date.now()) {
    store.dispatch(logoutUser());
  } else {
    store.dispatch({ type: SET_AUTHENTICATED });
    axios.defaults.headers.common["Authorization"] = token;
    store.dispatch(getUserData());
  }
}

const Routes = () => (
  <div>
    <NavigationBar />
    <Switch>
      <Route exact path="/" component={Home} />
      <div className="container">
        <Route exact path="/searchResults" component={SearchResults} />
        <Route exact path="/getWedding/:weddingId" component={WeddingDetails} />
        <Route
          exact
          path="/conversation/:conversationId"
          component={Auth(Conversation)}
        />
      </div>
    </Switch>
  </div>
);

export default Routes;
