module.exports = {
  GET_WEDDINGS_ALL_COUNTRIES: "getWeddingsAllCountries",
  GET_WEDDINGS_BY_DATE_ALL_COUNTRIES: "getWeddingsByDateAllCountries",
  GET_WEDDINGS_BY_DATE_SPECIFIC_COUNTRY: "getWeddingsByDateSpecificCountry",
  GET_WEDDINGS_BY_SPECIFIC_COUNTRY: "getWeddingsBySpecificCountry",
  ALL_COUNTRIES: "All Countries",
  INVALID_DATE: "Invalid Date",
  NULL: "null"
};
