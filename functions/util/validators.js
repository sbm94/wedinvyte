var constants = require("../util/constants");

const isEmail = email => {
  const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (email.match(emailRegEx)) return true;
  else return false;
};

const isPassword = password => {
  const passwordRegEx = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
  if (password.match(passwordRegEx)) return true;
  else return false;
};

const isEmpty = string => {
  if (string.trim() === "") return true;
  else return false;
};

const isInvalidDate = string => {
  if (string === "null") {
    return true;
  } else return false;
};

exports.validateSignUpData = data => {
  let errors = {};

  if (isEmpty(data.email)) {
    errors.email = "must not be empty";
  } else if (!isEmail(data.email)) {
    errors.email = "must be a valid email address";
  }

  if (isEmpty(data.password)) {
    errors.password = "must not be empty";
  } else if (!isPassword(data.password)) {
    errors.password =
      "must include 1 lowercase, 1 uppercase, 1 numeric character, 1 special character, and be at least 8 characters long.";
  }
  if (data.password !== data.confirmPassword)
    errors.confirmPassword = "passwords do not match";
  if (isEmpty(data.firstName)) errors.firstName = "must not be empty";
  if (isEmpty(data.lastName)) errors.lastName = "must not be empty";
  if (isEmpty(data.birthday)) errors.birthday = "must not be empty";
  if (isEmpty(data.confirmPassword))
    errors.confirmPassword = "must not be empty";

  return {
    errors,
    valid: Object.keys(errors).length === 0 ? true : false
  };
};

exports.validateLoginData = data => {
  let errors = {};
  if (isEmpty(data.email)) errors.email = "must not be empty";
  if (isEmpty(data.password)) errors.password = "must not be empty";

  return {
    errors,
    valid: Object.keys(errors).length === 0 ? true : false
  };
};

exports.validateWeddingSearchParams = params => {
  let queryType = {};
  let fromDate = params.fromDate.toString();
  let toDate = params.toDate.toString();
  let country = params.country.toString();
  if (
    (isInvalidDate(fromDate) || isInvalidDate(toDate)) &&
    country === constants.ALL_COUNTRIES
  ) {
    queryType.query = constants.GET_WEDDINGS_ALL_COUNTRIES;
  } else if (
    !isInvalidDate(fromDate) &&
    !isInvalidDate(toDate) &&
    country === constants.ALL_COUNTRIES
  ) {
    queryType.query = constants.GET_WEDDINGS_BY_DATE_ALL_COUNTRIES;
  } else if (
    !isInvalidDate(fromDate) &&
    !isInvalidDate(toDate) &&
    country !== constants.ALL_COUNTRIES
  ) {
    queryType.query = constants.GET_WEDDINGS_BY_DATE_SPECIFIC_COUNTRY;
  } else if (
    (isInvalidDate(fromDate) || isInvalidDate(toDate)) &&
    country !== constants.ALL_COUNTRIES
  ) {
    queryType.query = constants.GET_WEDDINGS_BY_SPECIFIC_COUNTRY;
  }

  return queryType;
};
