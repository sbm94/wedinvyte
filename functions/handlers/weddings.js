const { db } = require("../util/admin");
var constants = require("../util/constants");

const { validateWeddingSearchParams } = require("../util/validators");

exports.createWedding = (request, response) => {
  const newWedding = {
    city: request.body.city,
    country: request.body.country,
    state: request.body.state,
    user: request.user.email
  };

  db.collection("weddings")
    .add(newWedding)
    .then(doc => {
      return response.json({
        message: `document ${doc.id} created successfully`
      });
    })
    .catch(err => {
      response.status(500).json({ error: "something went wrong" });
      console.error(err);
    });
};

exports.getWeddings = async function(request, response) {
  const weddingSearchParams = {
    fromDate:
      request.body.fromDate === constants.NULL
        ? constants.NULL
        : new Date(request.body.fromDate),
    toDate:
      request.body.toDate === constants.NULL
        ? constants.NULL
        : new Date(request.body.toDate),
    maxGuests: request.body.guests,
    country: request.body.country
  };

  const queryType = validateWeddingSearchParams(weddingSearchParams);
  var weddingResults = [];

  switch (queryType.query) {
    case constants.GET_WEDDINGS_ALL_COUNTRIES:
      weddingResults = await getWeddingsAllCountries(
        weddingSearchParams.maxGuests
      ).then(result => result);
      return response.json(weddingResults);
    case constants.GET_WEDDINGS_BY_DATE_ALL_COUNTRIES:
      weddingResults = await getWeddingsByDateAllCountries(
        weddingSearchParams.fromDate,
        weddingSearchParams.toDate,
        weddingSearchParams.maxGuests
      ).then(result => result);
      return response.json(weddingResults);
    case constants.GET_WEDDINGS_BY_DATE_SPECIFIC_COUNTRY:
      weddingResults = await getWeddingsByDateSpecificCountry(
        weddingSearchParams.fromDate,
        weddingSearchParams.toDate,
        weddingSearchParams.maxGuests,
        weddingSearchParams.country
      ).then(result => result);
      return response.json(weddingResults);
    case constants.GET_WEDDINGS_BY_SPECIFIC_COUNTRY:
      weddingResults = await getWeddingsBySpecificCountry(
        weddingSearchParams.maxGuests,
        weddingSearchParams.country
      ).then(result => result);
      return response.json(weddingResults);
    default:
      return response.json(weddingResults);
  }
};

async function getWeddingsAllCountries(maxGuests) {
  let weddings = [];
  try {
    const snapshot = await db.collection("weddings").get(); //filter by maxGuests and then get top
    snapshot.forEach(doc => {
      wedding = doc.data();
      if (wedding.maxGuests <= maxGuests) {
        weddings.push({
          ...doc.data(),
          weddingId: doc.id
        });
      }
    });
    return weddings;
  } catch (err) {
    console.error(err);
  }
}

async function getWeddingsByDateAllCountries(fromDate, toDate, maxGuests) {
  try {
    let weddings = [];
    const snapshot = await db
      .collection("weddings")
      .where("toDate", "<=", toDate)
      .orderBy("toDate")
      .startAfter(fromDate)
      .get();
    snapshot.forEach(doc => {
      wedding = doc.data();
      if (wedding.maxGuests <= maxGuests) {
        weddings.push({
          ...doc.data(),
          weddingId: doc.id
        });
      }
    });
    return weddings;
  } catch (err) {
    console.error(err);
  }
}

async function getWeddingsByDateSpecificCountry(
  fromDate,
  toDate,
  maxGuests,
  country
) {
  try {
    let weddings = [];
    const snapshot = await db
      .collection("weddings")
      .where("toDate", "<=", toDate)
      .where("country", "==", country)
      .orderBy("toDate")
      .startAfter(fromDate)
      .get();
    snapshot.forEach(doc => {
      wedding = doc.data();
      if (wedding.maxGuests <= maxGuests) {
        weddings.push({
          ...doc.data(),
          weddingId: doc.id
        });
      }
    });
    return weddings;
  } catch (err) {
    console.error(err);
  }
}

async function getWeddingsBySpecificCountry(maxGuests, country) {
  try {
    let weddings = [];
    const snapshot = await db
      .collection("weddings")
      .where("country", "==", country)
      .orderBy("toDate")
      .get();
    snapshot.forEach(doc => {
      wedding = doc.data();
      if (wedding.maxGuests <= maxGuests) {
        weddings.push({
          ...doc.data(),
          weddingId: doc.id
        });
      }
    });
    return weddings;
  } catch (err) {
    console.error(err);
  }
}

exports.getWedding = (request, response) => {
  let weddingData = {};
  db.doc(`/weddings/${request.params.weddingId}`)
    .get()
    .then(doc => {
      if (!doc.exists) {
        return response.status(400).json({ error: "Wedding not found" });
      }
      weddingData = doc.data();
      weddingData.weddingId = doc.id;
      return response.json(weddingData);
    })
    .catch(err => {
      console.error(err);
      response.status(500).json({ error: err.code });
    });
};

exports.getCountries = (request, response) => {
  let countries = new Set();
  db.collection("weddings")
    .get()
    .then(data => {
      data.forEach(doc => {
        countries.add(doc.data().country);
      });
      let countriesArray = Array.from(countries);
      return response.json(countriesArray);
    })
    .catch(err => {
      console.error(err);
      response.status(500).json({ error: err.code });
    });
};

exports.getItinerary = async function(request, response) {
  let weddingId = request.body.weddingId;
  console.log(weddingId);
  let itinerary = [];
  db.collection("itineraries")
    .where("weddingId", "==", weddingId)
    .get()
    .then(data => {
      data.forEach(doc => {
        itinerary.push(doc.data());
      });
      return response.json(itinerary);
    })
    .catch(err => {
      console.error(err);
      response.status(500).json({ error: err.code });
    });
};
