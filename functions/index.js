const functions = require("firebase-functions");
const app = require("express")();

const cors = require("cors");
app.use(cors());

const {
  createWedding,
  getWeddings,
  getWedding,
  getCountries,
  getItinerary
} = require("./handlers/weddings");
const {
  signUp,
  login,
  uploadImage,
  getAuthenticatedUser
} = require("./handlers/users");
const fbAuth = require("./util/fbAuth");

//Wedding routes
app.post("/api/getWeddings", getWeddings);
app.post("/api/createWedding", fbAuth, createWedding);
app.get("/api/getWedding/:weddingId", getWedding);
app.get("/api/getCountries", getCountries);
app.post("/api/getItinerary", getItinerary);
//users routes
app.post("/api/signUp", signUp);
app.post("/api/login", login);
app.post("/api/users/image", fbAuth, uploadImage);
app.get("/api/user", fbAuth, getAuthenticatedUser);

exports.api = functions.https.onRequest(app);
