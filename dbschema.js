//This file does not impact the code. It is used more for reference to see what the Data Schema looks like.

let db = {
    weddings: [{
        city: "Bangalore",
        state: "Karnataka",
        country: "India",
        fromDate: "2019-03-15T11:47:40.0922", //string, not a timestamp. using new Date().toISOString()
        toDate: "2019-03-12T11:47:40.0922",
        createdAt: "2019-02-15T11:47:40.0922",
        userId: "xuba4dnsSRfsa" //reference to user id who posted the wedding
           
    
    }]
}